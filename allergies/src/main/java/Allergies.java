import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Allergies
{

  private final int score;


  Allergies(int score)
  {
    this.score = score;
  }


  //
  // key to this solution was using bitwise &
  // to determine allergen hit
  //
  // 00000101 5 is a hit on 1 and 4 
  //
  //
  boolean isAllergicTo(Allergen allergen)
  {
    return (this.score & allergen.getScore()) > 0;
  }


  List<Allergen> getList()
  {
    return Arrays.stream(Allergen.values()).
      filter(allergen-> isAllergicTo(allergen)).
      collect(Collectors.toList());
  }


}
