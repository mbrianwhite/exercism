enum Allergen
{

  //
  // define enums in powers of 2
  // as was defined by AllergiesTest
  // which hinted at bitwise operation as a solution
  //

  EGGS(1),
  PEANUTS(2),
  SHELLFISH(4),
  STRAWBERRIES(8),
  TOMATOES(16),
  CHOCOLATE(32),
  POLLEN(64),
  CATS(128);


  private final int score;


  Allergen(int score)
  {
    this.score = score;
  }

  
  public int getScore()
  {
    return score;
  }
}
