import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PigLatinTranslator
{

    //
    // constants
    //
    static final String AY = "ay";

    //
    // pattern definitions
    //
    private static final Pattern VOWEL_PATTERN = Pattern.compile("(.*?)([aeoiu].*)");
    private static final Pattern QU_PATTERN = Pattern.compile("([^aeio]*qu)(.*)");
    private static final Pattern DAIQUIRI_PATTERN = Pattern.compile("(.*?)(([aeiou]+qu)(.*))+");    // daiquiri  ->  aiquiriday
    private static final Pattern YT_XR_PATTERN = Pattern.compile("^yt|xr(.*)");



    public static String translate(String input)
    {
        return Stream.of(input.split(" "))
          .map(w -> pigLatinWordFor(w))
          .collect(Collectors.joining(" "));
    }


    //
    // look for special cases first, qu, yt, xr
    // else just find where vowels start
    //
    private static String pigLatinWordFor(String input)
    {
        Matcher matcher = DAIQUIRI_PATTERN.matcher(input);
        if(matcher.find())
        {
            return matcher.group(2) + matcher.group(1) + AY;
        }
        else if((matcher=QU_PATTERN.matcher(input)).find())
        {
            return matcher.group(2) + matcher.group(1) + AY;
        }
        else if(YT_XR_PATTERN.matcher(input).find())
        {
            return input + AY;
        }
        else if((matcher = VOWEL_PATTERN.matcher(input)).find())
        { 
            return matcher.group(2) + matcher.group(1) + AY;
        }

        return input;
    }
}